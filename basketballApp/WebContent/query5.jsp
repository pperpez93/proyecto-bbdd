<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="mipk.beanDB"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Partidos seg�n equipo</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Basketball Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="assets/css/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css">
<!-- Custom Theme files -->
<link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="assets/css/style.css" rel='stylesheet' type='text/css' />	
<script src="assets/js/jquery.min.js"> </script>
<script type="text/javascript" src="assets/js/move-top.js"></script>
<script type="text/javascript" src="assets/js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
</head>
<body> 
 <div class="container">
  <div class="header" id="home">
	 <div class="subhead white">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp"><h1>NBA Playoffs<span> 2015</span></h1> </a>
			</div>
			<!--/.navbar-header-->
		
		<div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="consultas.jsp">Consultas</a></li>
					<li><a href="galer�a.jsp">Galer�a</a></li>
				</ul>
			</div>
			<!--/.navbar-collapse-->
	 <!--/.navbar-->
	</nav>
  </div>
 </div>
   <!--/start-banner-->
    <div class="banner two">
  </div>
    <!--//end-banner-->
     <!--/start-main-->
          <div class="main-content">
		      <!--/soccer-inner-->
	                 <div class="soccer-inner">
					  <!--/soccer-left-part-->
					  <div class="col-md-8 soccer-left-part blog" style="text-align: center; justify-content: center; display: flex;">
					   <div class="blogs">
					        <div class="blog-grid">
							    <a href="#"> <img src="assets/images/query5.jpg" class="img-responsive" alt="" /></a>
								 <div class="blog-info second">
									   <a class="news" href="#"> PARTIDOS SEG�N EQUIPO</a>
									  <p>Echa un vistazo a los partidos de cada equipo</p>
										  </div>
						     </div>
						<%
							String query = "select id_equipo, nombre from Equipos";
							beanDB basededatos = new beanDB();
							String[][] tablares = basededatos.resConsultaSelectA3(query);
						%>		
						<br>

						<div class="single-bottom"
							style="text-align: left; justify-content: center;">
							<p>Selecciona un equipo a continuaci�n para ver los partidos que ha disputado durante esta temporada en los Playoffs. </p>
						</div>

						<br>

						<div>
							<form name="introgrupo" action="query5.jsp" method="get">
								<select name="equipo" onchange="submit()">
									<option value="">Seleccione un equipo...</option>
									<%
										for (int i = 0; i < tablares.length; i++) {
									%>
									<option value="<%=tablares[i][0]%>"><%=tablares[i][1]%></option>
									<%
										}
									%>
								</select>
							</form>
						</div>
												
						<%
						String equipoRecibido = "";
							try {
								equipoRecibido = request.getParameter("equipo").toString();
							} catch (Exception e) {
							}
							String query2 = "select fecha, Local.nombre, puntos_local, Visitante.nombre, puntos_visitante from Equipos Local join Partidos on (Local.id_equipo=equipo_local) join Equipos Visitante on (Visitante.id_equipo=equipo_visitante) where Local.id_equipo = "+ equipoRecibido+" or Visitante.id_equipo = "+ equipoRecibido;
							String[][] tablares2 = basededatos.resConsultaSelectA3(query2);

						if (tablares2 != null) {%>
						<div class="fixtures">
							<h3 class="tittle"></h3>
							<table>
								<thead>
									<tr>
										<th>Fecha</th>
										<th>Equipo Local</th>
										<th>Pts</th>
										<th>Equipo Visitante</th>
										<th>Pts</th>
									</tr>
								</thead>
								<tbody>
										<%
								for (int i = 0; i < tablares2.length; i++) {
								%><tr>
									<%
									for (int j = 0; j < tablares2[i].length; j++) {
									%>
									<td><%=tablares2[i][j]%></td>
									<%
									}
									%>
									</tr>
								<%
								}
								%>
								</tbody>
							</table>
						</div>
						<%}%>
					</div>
				<div class="clearfix"></div>

				</div>
							      
					<!--banner Slider starts Here-->
					<script src="assets/js/responsiveslides.min.js"></script>
						 <script>
								// You can also use "$(window).load(function() {"
								$(function() {
									// Slideshow 3
									$("#slider3")
											.responsiveSlides(
													{
														auto : true,
														pager : false,
														nav : true,
														speed : 500,
														namespace : "callbacks",
														before : function() {
															$('.events')
																	.append(
																			"<li>before event fired.</li>");
														},
														after : function() {
															$('.events')
																	.append(
																			"<li>after event fired.</li>");
														}
													});

								});
							</script>
					 </div>
					<!--//soccer-left-part-->
							<!--/soccer-right-part-->
					<div class="col-md-4 soccer-right-part">
			                     <div class="modern">
							  <h4 class="side">Jugadores destacados</h4>
							  <div id="example1">
								<div id="owl-demo" class="owl-carousel text-center">
								  <div class="item">
							
									<img class="img-responsive lot" src="assets/images/curry.jpg" alt=""/>
								 </div>
								 <div class="item">
							
									<img class="img-responsive lot" src="assets/images/lebron.jpg" alt=""/>
								</div>
								<div class="item">
							
									<img class="img-responsive lot" src="assets/images/howard.jpg" alt=""/>
								</div>
								<div class="item">
							
									<img class="img-responsive lot" src="assets/images/iguodala.jpg" alt=""/>
							</div>
							<div class="item">
							
									<img class="img-responsive lot" src="assets/images/duncan.jpg" alt=""/>
							</div>
						</div>
				    </div>
				<!-- requried-jsfiles-for owl -->
										<script src="assets/js/owl.carousel.js"></script>
										  <script>
										  $(document).ready(function() {
											   $("#owl-demo").owlCarousel({
												items :1,
												lazyLoad : true,
												autoPlay : false,
												navigation : true,
												navigationText :  true,
												pagination : false,
												responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems: 2
										}, 
										landscape: { 
											changePoint:640,
											visibleItems: 2
										},
										tablet: { 
											changePoint:768,
											visibleItems: 3
										}
									}
												});
										  });
										</script>
						<!-- //requried-jsfiles-for owl -->
						</div>
				 <!--//accordation_menu-->
        
		 <!--//accordation_menu-->
		 <!--/top-news-->
			  <div class="top-news">
								 <h4 class="side">Partidos Destacados</h4>
							      <div class="top-inner">
								     <div class="top-text">
										 <a href="#"><img src="assets/images/game1.jpg" class="img-responsive" alt=""/></a>
										 <h5 class="top"><a href="#">San Antonio Spurs VS Los Angeles Clippers</a></h5>
										 <p>On Jun 25 <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>0 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>56 </a></p>
								     </div>
									  <div class="top-text two">
										 <a href="#"><img src="assets/images/game2.jpg" class="img-responsive" alt=""/></a>
										 <h5 class="top"><a href="#">Golden State Warriors VS Cleveland Cavaliers</a></h5>
										 <p>On Jun 25 <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>0 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>56 </a></p>
										
								     </div>
								  </div>
	                            </div>
							<!--//top-news-->
										<div class="connect">
											<h4 class="side">Siguenos</h4>
											  <ul class="stay">
												<li class="c5-element-facebook"><a href="#"><span class="icon"></span><span class="text">Facebook</span></a></li>
												<li class="c5-element-twitter"><a href="#"><span class="icon1"></span><span class="text">Twitter</span></a></li>
												 <li class="c5-element-gg"><a href="#"><span class="icon2"></span><span class="text">Google</span></a></li>
												
											  </ul>
										  </div>
										  	<!--//connect-->
		            </div>
			         <!--//soccer-right-part-->
		           <div class="clearfix"> </div>
	           </div>
			</div>
		<!--//soccer-inner-->
	 </div>
	<!--/start-footer-section-->

	<!--//end-footer-section-->
			<!--/start-copyright-section-->
				<div class="copyright">
						  <p>&copy; 2015 Basketball. All Rights Reserved | Design by <a href="http://w3layouts.com/">W3layouts</a> </p>
					</div>


				<!--start-smoth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>


<!--JS-->
<script type="text/javascript" src="assets/js/bootstrap-3.1.1.min.js"></script>

<!--//JS-->

</body>
</html>