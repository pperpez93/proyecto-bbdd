DROP DATABASE IF EXISTS Playoffs;
CREATE DATABASE Playoffs;
USE Playoffs;

CREATE TABLE Conferencias (
id_conferencia INT(11) PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(50) DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE Equipos (
id_equipo INT(11) PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(100) DEFAULT NULL,
conferencia_id INT(11) NOT NULL,
CONSTRAINT fk_equipo_conferencia FOREIGN KEY (conferencia_id) REFERENCES Conferencias (id_conferencia)
) ENGINE=InnoDB;

CREATE TABLE Partidos (
id_partido INT(11) PRIMARY KEY AUTO_INCREMENT,
fecha DATE NOT NULL,
equipo_visitante INT(11) NOT NULL,
puntos_visitante INT(11) DEFAULT NULL,
equipo_local INT(11) NOT NULL,
puntos_local INT(11) DEFAULT NULL,
CONSTRAINT fk_equipo_visitante FOREIGN KEY (equipo_visitante) REFERENCES Equipos (id_equipo),
CONSTRAINT fk_equipo_local FOREIGN KEY (equipo_local) REFERENCES Equipos (id_equipo)
) ENGINE=InnoDB;

CREATE TABLE Jugadores (
id_jugador INT(11) PRIMARY KEY AUTO_INCREMENT,
dorsal VARCHAR(10) DEFAULT NULL,
nombre VARCHAR(100) DEFAULT NULL,
posicion VARCHAR(5) DEFAULT NULL,
altura VARCHAR(10) DEFAULT NULL,
peso INT(11),
equipo_id INT(11) NOT NULL,
CONSTRAINT fk_jugador_equipo FOREIGN KEY (equipo_id) REFERENCES Equipos (id_equipo)
) ENGINE=InnoDB;

CREATE TABLE Estadisticas (
id_estadistica INT(11) PRIMARY KEY AUTO_INCREMENT,
jugador_id INT(11) NOT NULL,
rebotes_por_partido DOUBLE,
asistencias_por_partido DOUBLE,
robos_por_partido DOUBLE,
tapones_por_partido DOUBLE,
puntos_por_partido DOUBLE,
CONSTRAINT fk_estadistica_jugador FOREIGN KEY (jugador_id) REFERENCES Jugadores (id_jugador)
) ENGINE=InnoDB;

-- CONFERENCIAS --

INSERT INTO Playoffs.Conferencias (nombre) VALUES ('Este');
INSERT INTO Playoffs.Conferencias (nombre) VALUES ('Oeste');

-- EQUIPOS --

INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Dallas Mavericks', 2);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Houston Rockets', 2);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Los Angeles Clippers', 2);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('San Antonio Spurs', 1);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Golden State Warriors', 2);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Washington Wizards', 1);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('New Orleans Pelicans', 2);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Cleveland Cavaliers', 1);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Atlanta Hawks', 1);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Brooklyn Nets', 1);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Toronto Raptors', 1);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Chicago Bulls', 1);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Boston Celtics', 1);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Memphis Grizzlies', 2);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Portland Trail Blazers', 2);
INSERT INTO Playoffs.Equipos (nombre, conferencia_id) VALUES ('Milwaukee Bucks', 1);

-- JUGADORES --

-- Dallas Mavericks
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('7','Al-Farouq Aminu','SF','6-9', 215, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','J.J. Barea','PG','6-0',185, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('6','Tyson Chandler','C','7-1',240, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Jae Crowder','SF','6-6',235, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Monta Ellis','SG','6-3',185, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Raymond Felton','PG','6-1',205, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('20','Devin Harris','PG','6-3',192, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('55','Bernard James','C','6-10',240, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('24','Richard Jefferson','SF','6-7',233, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Ricky Ledo','SG','6-7',195, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('14','Jameer Nelson','PG','6-0',190, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('41','Dirk Nowitzki','PF','7-0',245, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('25','Chandler Parsons','SF','6-9',227, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Dwight Powell','PF','6-11',240, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Rajon Rondo','PG','6-1',186, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','Greg Smith','PF','6-10',250, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Amare Stoudemire','PF','6-10',245, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Charlie Villanueva','PF','6-11',232, 1);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('34','Brandan Wright','PF','6-10',210, 1);

-- Houston Rockets
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Trevor Ariza','SF','6-8',215,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Patrick Beverley','PG','6-1',185,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('10','Tarik Black','C','6-9',250,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('33','Corey Brewer','SF','6-9',186,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','Isaiah Canaan','PG','6-0',201,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('15','Clint Capela','C','6-10',240,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('30','Troy Daniels','SG','6-4',205,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Joey Dorsey','PF','6-8',268,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('32','Francisco Garcia','SF','6-7',195,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('13','James Harden','SG','6-5',220,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','Dwight Howard','C','6-11',265,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Nick Johnson','SG','6-3',202,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('6','Terrence Jones','PF','6-9',252,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('32','K.J. McDaniels','SG','6-6',205,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('20','Donatas Motiejunas','PF','7-0',222,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('16','Kostas Papanikolaou','SF','6-8',225,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Pablo Prigioni','PG','6-3',185,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('88','Alexey Shved','SG','6-6',190,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Josh Smith','PF','6-9',225,2);
INSERT INTO Playoffs.Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('31','Jason Terry','SG','6-2',185,2);

-- Los Angeles Clippers
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('22','Matt Barnes','SF','6-7',226,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('25','Reggie Bullock','SF','6-7',205,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Jamal Crawford','SG','6-5',195,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Jared Cunningham','SG','6-4',195,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','Glen Davis','PF','6-9',289,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('14','Chris Douglas-Roberts','SG','6-7',200,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Jordan Farmar','PG','6-2',180,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('32','Blake Griffin','PF','6-10',251,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Jordan Hamilton','SF','6-7',220,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('10','Spencer Hawes','PF','7-1',245,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('14','Lester Hudson','SG','6-3',190,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('31','Dahntay Jones','SF','6-6',225,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('6','DeAndre Jordan','C','6-11',265,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Chris Paul','PG','6-0',175,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','J.J. Redick','SG','6-4',190,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('25','Austin Rivers','PG','6-4',200,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Nate Robinson','PG','5-9',180,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('15','Hedo Turkoglu','SF','6-10',220,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('13','Ekpe Udoh','PF','6-10',240,3);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('30','C.J. Wilcox','SG','6-5',195,3);

-- San Antonio Spurs
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Kyle Anderson','SF','6-9',230,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Jeff Ayres','PF','6-9',240,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('16','Aron Baynes','C','6-10',260,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Marco Belinelli','SG','6-5',210,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('15','Matt Bonner','PF','6-10',235,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('23','Austin Daye','SF','6-11',220,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('33','Boris Diaw','PF','6-8',250,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('21','Tim Duncan','PF','6-11',250,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('20','Manu Ginobili','SG','6-6',205,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('14','Danny Green','SG','6-6',215,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('41','JaMychal Green','PF','6-9',227,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Cory Joseph','PG','6-3',190,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Kawhi Leonard','SF','6-7',230,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Patrick Mills','PG','6-0',185,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Tony Parker','PG','6-2',185,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('22','Tiago Splitter','C','6-11',245,4);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('55','Reggie Williams','SF','6-6',205,4);

-- Golden State Warriors
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('19','Leandro Barbosa','SG','6-3',194,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('40','Harrison Barnes','SF','6-8',210,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','Andrew Bogut','C','7-0',260,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('30','Stephen Curry','PG','6-3',185,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('31','Festus Ezeli','C','6-11',255,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('23','Draymond Green','PF','6-7',230,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('7','Justin Holiday','SG','6-6',185,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Andre Iguodala','SG','6-6',207,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Ognjen Kuzmic','C','7-1',251,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('10','David Lee','PF','6-9',245,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('34','Shaun Livingston','PG','6-7',182,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('20','James Michael McAdoo','PF','6-9',230,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','Brandon Rush','SG','6-6',210,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Marreese Speights','PF','6-10',255,5);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Klay Thompson','SG','6-7',205,5);

-- Washington Wizards
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Bradley Beal','SG','6-5',207,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('45','DeJuan Blair','PF','6-7',270,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Rasual Butler','SF','6-7',215,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Will Bynum','SG','6-0',185,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('90','Drew Gooden','PF','6-10',250,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','Marcin Gortat','C','6-11',240,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('42','Nene Hilario','PF','6-11',250,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('43','Kris Humphries','PF','6-9',235,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('24','Andre Miller','PG','6-3',200,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Toure Murry','SG','6-5',195,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('34','Paul Pierce','SF','6-7',235,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('22','Otto Porter','SF','6-8',198,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('14','Glen Rice','SG','6-6',206,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('13','Kevin Seraphin','C','6-10',278,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('7','Ramon Sessions','PG','6-3',190,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('17','Garrett Temple','SG','6-6',195,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','John Wall','PG','6-4',195,6);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Martell Webster','SF','6-7',230,6);

-- New Orleans Pelicans
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('42','Alexis Ajinca','C','7-2',248,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('33','Ryan Anderson','PF','6-10',240,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Omer Asik','C','7-0',255,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Luke Babbitt','SF','6-9',225,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('30','Norris Cole','PG','6-2',175,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','Dante Cunningham','PF','6-8',230,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('23','Anthony Davis','PF','6-10',253,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('16','Toney Douglas','PG','6-2',195,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Tyreke Evans','SG','6-6',220,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('32','Jimmer Fredette','PG','6-2',195,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('10','Eric Gordon','SG','6-4',215,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Jrue Holiday','PG','6-4',205,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('24','Gal Mekel','PG','6-3',191,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Darius Miller','SF','6-8',235,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('20','Quincy Pondexter','SF','6-7',220,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('25','Austin Rivers','SG','6-4',200,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('15','John Salmons','SF','6-7',210,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Russ Smith','PG','6-0',165,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Elliot Williams','SG','6-5',190,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Jeff Withey','C','7-0',231,7);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','Nate Wolters','PG','6-4',190,7);

-- Cleveland Cavaliers
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('89','Lou Amundson','PF','6-9',220,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Will Cherry','PG','6-0',185,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Matthew Dellavedova','SG','6-4',198,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','Joe Harris','SG','6-6',219,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('33','Brendan Haywood','C','7-0',268,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Kyrie Irving','PG','6-3',193,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('23','LeBron James','SF','6-8',250,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','James Jones','SF','6-8',218,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('53','Alex Kirk','C','7-0',245,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','Kevin Love','PF','6-10',251,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('31','Shawn Marion','SF','6-7',220,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('18','Mike Miller','SF','6-8',218,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('20','Timofey Mozgov','C','7-1',275,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Kendrick Perkins','C','6-10',270,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('21','A.J. Price','PG','6-2',181,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','Iman Shumpert','SG','6-5',220,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','J.R. Smith','SG','6-6',225,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('13','Tristan Thompson','PF','6-9',238,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('17','Anderson Varejao','C','6-10',273,8);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Dion Waiters','SG','6-4',225,8);

-- Atlanta Hawks
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('6','Pero Antic','PF','6-11',260,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('24','Kent Bazemore','SG','6-5',201,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('7','Elton Brand','PF','6-8',275,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','DeMarre Carroll','SF','6-8',212,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Austin Daye','SF','6-11',220,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('15','Al Horford','C','6-10',245,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','John Jenkins','SG','6-4',215,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('26','Kyle Korver','SG','6-7',212,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Shelvin Mack','PG','6-3',203,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','Paul Millsap','PF','6-8',246,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('31','Mike Muscala','PF','6-11',240,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('33','Adreian Payne','PF','6-10',237,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('17','Dennis Schroder','PG','6-1',172,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('32','Mike Scott','PF','6-8',237,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('25','Thabo Sefolosha','SF','6-7',220,9);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','Jeff Teague','PG','6-2',186,9);

-- Brooklyn Nets
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('6','Alan Anderson','SG','6-6',220,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('44','Bojan Bogdanovic','SF','6-8',216,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('22','Markel Brown','SG','6-3',190,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('55','Earl Clark','PF','6-10',225,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('20','Brandon Davies','PF','6-10',240,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Kevin Garnett','PF','6-11',240,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('13','Jorge Gutierrez','PG','6-3',191,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','Jarrett Jack','PG','6-3',200,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('21','Cory Jefferson','PF','6-9',218,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('7','Joe Johnson','SG','6-7',240,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Jerome Jordan','C','7-0',253,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('10','Sergey Karasev','SG','6-7',208,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('47','Andrei Kirilenko','SF','6-9',220,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Brook Lopez','C','7-0',275,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('14','Darius Morris','PG','6-4',190,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Mason Plumlee','C','6-11',245,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('33','Mirza Teletovic','PF','6-9',242,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Deron Williams','PG','6-3',200,10);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('30','Thaddeus Young','PF','6-8',221,10);

-- Toronto Raptors
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Bruno Caboclo','SF','6-9',205,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('10','DeMar DeRozan','SG','6-7',220,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Landry Fields','SF','6-7',210,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('50','Tyler Hansbrough','PF','6-9',250,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('44','Chuck Hayes','C','6-6',240,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('15','Amir Johnson','PF','6-9',240,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','James Johnson','PF','6-9',250,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('7','Kyle Lowry','PG','6-0',205,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('92','Lucas Nogueira','C','7-0',220,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('54','Patrick Patterson','PF','6-9',235,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('31','Terrence Ross','SF','6-7',195,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('34','Greg Stiemsma','C','6-11',260,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('17','Jonas Valanciunas','C','7-0',255,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('21','Greivis Vasquez','PG','6-6',217,11);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('23','Lou Williams','SG','6-1',175,11);

-- Chicago Bulls
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('41','Cameron Bairstow','PF','6-9',250,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','Aaron Brooks','PG','6-0',161,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('21','Jimmy Butler','SG','6-7',220,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('34','Mike Dunleavy','SF','6-9',230,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('16','Pau Gasol','PF','7-0',250,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('22','Taj Gibson','PF','6-9',225,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','Kirk Hinrich','SG','6-4',190,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Doug McDermott','SF','6-8',225,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('44','Nikola Mirotic','PF','6-10',220,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('48','Nazr Mohammed','C','6-10',221,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('55','E Twaun Moore','SG','6-4',191,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('13','Joakim Noah','C','6-11',232,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Derrick Rose','PG','6-3',190,12);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('20','Tony Snell','SF','6-7',200,12);

-- Boston Celtics
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('30','Brandon Bass','PF','6-8',250,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','Avery Bradley','SG','6-2',180,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('99','Jae Crowder','SF','6-6',235,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('70','Luigi Datome','SF','6-8',215,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Jeff Green','SF','6-9',235,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Jonas Jerebko','PF','6-10',231,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('28','Jameer Nelson','PG','6-0',190,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('41','Kelly Olynyk','C','7-0',238,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','Dwight Powell','PF','6-11',240,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('26','Phil Pressey','PG','5-11',175,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','Tayshaun Prince','SF','6-9',212,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('42','Shavlik Randolph','PF','6-10',240,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Rajon Rondo','PG','6-1',186,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('36','Marcus Smart','PG','6-4',220,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('7','Jared Sullinger','PF','6-9',260,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','Isaiah Thomas','PG','5-9',185,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','Marcus Thornton','SG','6-4',205,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Evan Turner','SG','6-7',220,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('45','Gerald Wallace','SF','6-7',215,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','Brandan Wright','PF','6-10',210,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('13','James Young','SG','6-6',215,13);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('44','Tyler Zeller','C','7-0',253,13);

-- Memphis Grizzlies
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Jordan Adams','SG','6-5',209,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Tony Allen','SG','6-4',213,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','Nick Calathes','SG','6-6',213,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('15','Vince Carter','SG','6-6',220,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Mike Conley','PG','6-1',175,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('33','Marc Gasol','C','7-1',255,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','JaMychal Green','PF','6-9',227,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('32','Jeff Green','SF','6-9',235,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('41','Kosta Koufos','C','7-0',265,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Courtney Lee','SG','6-5',200,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('30','Jon Leuer','PF','6-10',228,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('27','Kalin Lucas','PG','6-1',195,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Quincy Pondexter','SG','6-7',220,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('21','Tayshaun Prince','SF','6-9',212,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('50','Zach Randolph','PF','6-9',260,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Russ Smith','PG','6-0',165,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Jarnell Stokes','PF','6-9',263,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('13','Tyrus Thomas','PF','6-9',215,14);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('19','Beno Udrih','PG','6-3',205,14);

-- Portland Trail Blazzers
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('4','Arron Afflalo','SG','6-5',210,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','LaMarcus Aldridge','PF','6-11',240,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Will Barton','SG','6-6',175,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('88','Nicolas Batum','SF','6-8',200,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Steve Blake','PG','6-3',172,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('18','Victor Claver','SF','6-9',224,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('23','Allen Crabbe','SG','6-6',210,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('10','Tim Frazier','PG','6-1',170,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('19','Joel Freeland','C','6-10',250,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('33','Alonzo Gee','SF','6-6',225,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('35','Chris Kaman','C','7-0',265,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Meyers Leonard','C','7-1',245,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','Damian Lillard','PG','6-3',195,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('42','Robin Lopez','C','7-0',255,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('2','Wesley Matthews','SG','6-5',220,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','C.J. McCollum','SG','6-4',200,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('41','Thomas Robinson','PF','6-10',237,15);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('1','Dorell Wright','SF','6-7',200,15);

-- Milwaukee Bucks
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('34','Giannis Antetokounmpo','SG','6-11',222,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('19','Jerryd Bayless','PG','6-3',200,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Michael Carter-Williams','PG','6-6',190,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('9','Jared Dudley','SG','6-7',225,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Tyler Ennis','PG','6-3',194,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('13','Jorge Gutierrez','PG','6-3',191,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('31','John Henson','C','6-11',229,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('7','Ersan Ilyasova','PF','6-10',235,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('23','Chris Johnson','SF','6-6',206,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('11','Brandon Knight','PG','6-3',189,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('5','Kendall Marshall','PG','6-4',200,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('6','Kenyon Martin','PF','6-9',234,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('0','O.J. Mayo','SG','6-5',210,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('22','Khris Middleton','SF','6-8',234,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('3','Johnny O Bryant','PF','6-9',257,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('27','Zaza Pachulia','C','6-11',270,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('12','Jabari Parker','SF','6-8',250,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('21','Miles Plumlee','C','6-11',249,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('8','Larry Sanders','C','6-11',235,16);
insert into Jugadores (dorsal, nombre, posicion, altura, peso, equipo_id) VALUES ('6','Nate Wolters','PG','6-4',190,16);

-- PARTIDOS --
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-18',16,91,12,103);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-18',7,99,5,106);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-18',1,108,2,118);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-18',6,93,11,86);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-19',10,92,9,99);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-19',13,100,8,113);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-19',4,92,3,107);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-19',15,86,14,100);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-20',16,82,12,91);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-20',7,87,5,97);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-21',13,91,8,99);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-21',1,99,2,111);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-21',6,117,11,106);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-22',10,91,9,96);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-22',4,111,3,107);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-22',15,82,14,97);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-23',8,103,13,95);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-23',12,113,16,106);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-23',5,123,7,119);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-24',2,130,1,128);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-24',3,73,4,100);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-24',11,99,6,106);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-25',9,83,10,91);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-25',12,90,16,92);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-25',5,109,7,98);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-25',14,115,15,109);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-26',8,101,13,93);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-26',2,109,1,121);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-26',3,114,4,105);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-26',11,94,6,125);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-27',9,115,10,120);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-27',16,94,12,88);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-27',14,92,15,99);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-28',1,94,2,103);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-28',4,111,3,107);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-29',10,97,9,107);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-29',15,93,14,99);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-30',12,120,16,66);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-04-30',3,102,4,96);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-01',9,111,10,87);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-02',4,109,3,111);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-03',6,104,9,98);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-03',14,86,5,101);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-04',12,99,8,92);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-04',3,117,2,101);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-05',6,90,9,106);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-05',14,97,5,90);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-06',12,91,8,106);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-06',3,109,2,115);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-08',8,96,12,99);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-08',2,99,3,124);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-09',5,89,14,99);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-09',9,101,6,103);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-10',8,86,12,84);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-10',2,95,3,128);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-11',5,101,14,84);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-11',9,106,6,101);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-12',12,101,8,106);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-12',3,103,2,124);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-13',6,81,9,82);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-13',14,78,5,98);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-14',8,94,12,73);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-14',2,119,3,107);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-15',5,108,14,95);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-15',9,94,6,91);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-17',3,100,2,113);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-19',2,106,5,110);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-20',8,97,9,89);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-21',2,98,5,99);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-22',8,94,9,82);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-23',5,115,2,80);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-24',9,111,8,114);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-25',5,115,2,128);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-26',9,88,8,118);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-05-27',2,90,5,104);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-06-04',8,100,5,108);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-06-07',8,95,5,93);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-06-09',5,91,8,96);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-06-11',5,103,8,82);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-06-14',8,91,5,104);
insert into Partidos (fecha, equipo_visitante, puntos_visitante, equipo_local, puntos_local) VALUES ('2015-06-16',5,105,8,97);

-- ESTADISTICAS (Promedio por partido)--

insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (13,6.0,2.0,0.0,0.0,10.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (12,10.4,2.4,0.4,0.4,21.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (3,10.8,0.2,0.6,1.2,10.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (2,4.8,7.4,0.8,0.0,11.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (1,7.2,1.2,2.0,1.6,11.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (15,1.0,3.0,0.0,0.0,9.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (7,2.0,1.0,0.5,0.0,6.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (17,3.2,0.6,0.2,0.6,7.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (9,0.5,0.3,0.5,0.0,3.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (6,2.3,1.3,0.0,0.0,3.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (18,2.6,0.6,0.2,0.2,6.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (8,0.0,0.0,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (14,0.5,0.5,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (16,0.0,0.0,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (20,6.4,2.6,1.8,0.1,13.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (29,5.7,7.5,1.6,0.4,27.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (30,14.0,1.2,1.4,2.3,16.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (39,2.2,2.8,0.9,0.1,9.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (23,2.8,1.1,0.6,0.2,11.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (32,4.8,1.0,0.5,0.7,10.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (38,5.6,2.7,0.5,1.0,13.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (36,1.1,2.3,0.9,0.0,3.1);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (25,2.5,0.3,0.2,0.5,3.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (31,0.4,0.4,0.0,0.1,1.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (35,0.3,0.0,0.0,0.0,0.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (27,0.7,0.3,0.0,0.0,0.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (47,12.7,6.1,1.0,1.0,25.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (54,2.1,1.7,0.7,0.4,14.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (53,4.4,8.8,1.8,0.3,22.1);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (52,13.4,1.1,1.1,2.4,13.1);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (40,5.1,1.6,1.4,0.7,7.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (42,2.1,1.9,0.9,0.2,12.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (55,1.7,1.1,0.7,0.3,8.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (44,1.9,0.2,0.4,0.4,2.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (49,1.6,0.6,0.3,0.4,2.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (50,0.1,1.0,0.3,0.1,2.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (57,0.6,0.4,0.4,0.1,0.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (58,0.8,0.0,0.0,0.0,0.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (51,0.1,0.0,0.2,0.0,0.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (67,11.1,3.3,1.3,1.4,17.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (72,7.4,2.6,1.1,0.6,20.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (74,3.3,3.6,0.3,0.0,10.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (69,3.1,2.1,1.0,1.0,8.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (66,6.1,3.6,0.7,0.4,11.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (68,3.4,4.6,0.6,0.9,8.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (75,4.4,1.3,0.6,0.1,3.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (63,1.9,1.4,0.3,0.0,9.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (73,2.7,1.1,0.3,0.0,10.1);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (62,2.5,0.3,0.0,0.0,2.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (71,0.3,0.0,0.0,0.3,2.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (64,0.9,0.1,0.0,0.1,0.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (61,1.0,0.7,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (80,5.0,6.4,1.9,0.1,28.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (82,10.1,5.2,1.8,1.2,13.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (91,3.9,2.6,0.8,0.9,18.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (78,5.2,1.5,0.8,0.5,10.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (84,4.5,3.6,1.2,0.3,10.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (79,8.1,1.9,0.6,1.8,4.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (87,2.4,1.8,0.4,0.2,5.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (77,1.3,0.9,0.3,0.0,5.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (81,3.1,0.3,0.1,0.5,3.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (86,2.6,0.6,0.2,0.2,3.1);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (90,2.1,0.4,0.4,0.3,3.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (89,1.0,0.0,0.0,0.0,1.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (83,0.2,0.2,0.0,0.0,0.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (88,0.8,0.0,0.0,0.2,0.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (92,5.5,4.6,1.6,0.7,23.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (108,4.7,11.9,1.4,1.4,17.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (103,8.0,1.8,1.2,0.2,10.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (97,8.8,2.2,0.6,1.1,12.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (102,4.2,0.9,0.6,0.7,14.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (98,6.6,1.5,0.9,0.3,7.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (106,2.4,2.3,0.4,0.1,7.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (96,5.5,0.8,0.2,1.0,6.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (105,3.2,0.3,0.3,0.2,5.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (95,1.0,1.0,0.7,0.3,6.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (107,0.8,0.3,0.5,0.0,1.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (99,3.0,0.0,0.0,0.0,2.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (109,0.0,0.0,0.0,0.0,2.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (94,0.5,0.5,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (116,11.0,2.0,1.3,3.0,31.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (120,2.3,3.8,0.5,0.5,18.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (118,5.0,5.0,1.3,0.3,10.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (124,5.0,3.0,1.8,0.0,7.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (114,1.8,1.8,0.0,0.3,8.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (111,4.3,2.3,0.0,0.5,10.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (112,7.3,1.5,1.3,0.0,2.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (115,4.5,0.5,0.8,1.0,5.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (121,1.0,4.3,0.7,0.3,6.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (110,0.3,0.3,0.3,0.0,2.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (137,11.3,8.5,1.7,1.1,30.1);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (148,10.8,0.5,0.3,1.2,9.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (136,3.6,3.8,1.3,0.8,19.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (146,4.9,1.2,1.3,0.8,9.1);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (147,4.7,1.2,0.9,0.6,12.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (140,7.0,2.5,0.3,0.5,14.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (143,7.3,0.7,0.4,1.8,10.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (133,2.1,2.7,0.5,0.0,7.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (138,1.5,0.5,0.4,0.2,4.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (142,1.1,0.0,0.1,0.1,1.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (141,1.0,0.2,0.3,0.0,0.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (144,1.1,0.0,0.0,0.4,1.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (134,0.2,0.2,0.0,0.0,1.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (135,0.0,0.0,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (158,5.0,2.4,1.4,1.1,11.1);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (160,8.7,3.4,1.6,0.9,15.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (154,6.1,2.0,1.1,0.3,14.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (166,3.2,6.7,1.5,0.4,16.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (156,8.6,3.7,0.8,1.4,14.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (152,3.3,0.8,0.7,0.6,5.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (163,1.8,3.9,0.6,0.0,9.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (164,4.2,0.5,0.5,0.0,4.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (151,2.9,0.3,0.1,0.2,4.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (161,1.8,0.1,0.1,0.3,4.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (159,1.1,1.0,0.8,0.0,3.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (157,0.8,0.0,0.0,0.0,2.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (153,0.3,0.0,0.0,0.0,0.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (176,7.7,4.8,1.2,0.0,16.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (180,9.0,0.8,0.7,2.2,19.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (168,3.8,1.7,0.7,0.3,10.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (184,6.2,5.5,1.3,0.0,11.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (185,7.2,2.7,0.8,0.2,10.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (174,4.2,4.5,1.2,0.2,12.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (167,3.5,1.2,0.7,0.2,11.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (182,1.3,0.3,0.7,0.3,2.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (170,1.0,0.0,0.5,0.0,3.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (183,1.3,0.0,0.3,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (181,0.0,0.0,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (169,1.0,0.5,0.0,0.0,3.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (177,2.0,0.0,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (187,6.3,5.8,1.5,0.0,20.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (193,5.5,4.8,1.3,0.0,12.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (191,7.0,1.0,0.3,0.8,11.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (196,1.5,1.0,0.8,1.0,7.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (195,3.5,1.3,0.8,0.0,10.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (198,9.3,0.5,0.5,0.3,11.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (200,1.8,1.3,1.5,0.0,12.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (199,1.8,3.0,0.5,0.0,7.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (189,1.5,0.8,0.5,0.3,1.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (192,1.0,0.5,0.0,0.0,2.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (197,0.0,0.0,0.0,0.0,5.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (203,5.6,3.2,2.4,0.8,22.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (213,4.8,6.5,1.2,0.5,20.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (212,11.0,3.2,0.8,1.2,5.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (204,4.0,2.6,0.8,0.4,10.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (205,9.4,3.1,0.5,2.1,14.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (206,5.5,1.0,0.3,1.0,7.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (209,2.7,0.8,0.5,0.5,5.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (214,1.5,0.5,0.0,0.3,3.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (207,0.5,1.1,0.3,0.1,2.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (202,1.5,0.9,0.3,0.1,4.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (210,1.7,0.0,0.0,0.3,1.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (208,0.7,0.3,0.0,0.0,1.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (211,1.0,0.0,0.7,0.0,1.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (216,3.8,0.8,0.8,0.0,12.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (230,3.0,7.0,0.8,0.0,17.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (232,7.3,4.8,0.8,0.0,10.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (217,5.0,2.0,1.0,0.8,10.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (236,4.5,0.5,0.5,0.3,8.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (228,2.8,1.3,0.3,0.3,9.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (215,2.0,2.5,0.3,0.8,5.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (229,7.0,0.3,0.0,0.8,12.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (220,3.5,0.3,0.5,0.3,2.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (222,1.3,0.5,0.5,0.5,4.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (218,0.3,0.3,0.0,0.0,1.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (233,1.0,0.0,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (224,0.5,0.0,0.0,0.5,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (242,10.3,4.5,0.9,1.7,19.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (251,8.5,2.1,0.5,0.0,15.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (246,2.5,2.2,1.1,0.0,13.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (246,1.1,5.0,1.4,0.0,14.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (238,5.2,1.5,2.4,1.1,6.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (244,4.7,1.7,0.5,0.5,8.9);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (240,4.3,1.0,0.6,0.2,6.3);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (255,2.0,2.1,0.5,0.0,7.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (239,1.8,1.8,1.0,0.1,3.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (245,3.5,0.4,0.4,0.5,3.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (237,0.3,0.0,0.3,0.0,1.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (247,1.3,0.0,0.0,0.0,1.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (243,0.6,0.0,0.2,0.2,0.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (252,0.0,1.0,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (259,8.6,5.2,0.2,0.2,14.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (257,11.2,1.8,0.4,2.4,21.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (268,4.0,4.6,0.4,0.6,21.6);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (271,4.0,0.4,1.2,0.2,17.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (269,4.4,0.6,0.2,1.0,5.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (267,6.6,1.0,0.4,0.4,7.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (256,2.3,0.7,0.0,0.0,1.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (262,1.5,0.5,1.0,0.5,5.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (266,4.7,1.0,0.0,0.0,3.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (260,0.2,1.6,0.0,0.2,1.4);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (264,0.5,0.0,0.5,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (265,1.0,1.0,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (263,0.0,0.0,0.0,0.0,0.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (287,3.7,2.0,2.3,0.5,15.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (274,7.0,2.7,0.5,1.5,11.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (276,4.5,4.8,1.2,1.0,12.2);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (286,3.3,3.0,1.2,0.2,9.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (280,8.0,0.7,0.8,1.7,8.8);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (281,3.8,0.5,0.8,0.5,8.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (289,6.7,1.5,1.7,0.5,6.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (275,2.5,3.0,0.3,0.3,6.5);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (277,1.8,1.3,2.0,0.3,6.7);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (278,4.0,3.0,0.0,0.0,5.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (291,6.0,1.0,0.0,1.0,3.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (288,3.0,0.0,0.0,0.0,6.0);
insert into Estadisticas (jugador_id, rebotes_por_partido, asistencias_por_partido, robos_por_partido, tapones_por_partido, puntos_por_partido) VALUES (279,1.0,1.0,0.0,0.0,2.0);
