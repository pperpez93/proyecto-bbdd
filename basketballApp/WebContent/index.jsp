<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Playoffs 2015</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Basketball Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="assets/css/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css">
<!-- Custom Theme files -->
<link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="assets/css/style.css" rel='stylesheet' type='text/css' />	
<script src="assets/js/jquery.min.js"> </script>
<script type="text/javascript" src="assets/js/move-top.js"></script>
<script type="text/javascript" src="assets/js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
</head>
<body> 
 <div class="container">
  <div class="header" id="home">
	 <div class="subhead white">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp"><h1>NBA Playoffs<span> 2015</span></h1> </a>
			</div>
			<!--/.navbar-header-->
		
			<div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="consultas.jsp">Consultas</a></li>
					<li><a href="galeria.jsp">Galer�a</a></li>
				</ul>
			</div>
			<!--/.navbar-collapse-->
	 <!--/.navbar-->
	</nav>
  </div>
 </div>
   <!--/start-banner-->
  <div class="banner">
	       <div class="banner-inner">
						<div class="callbacks_container">
						<ul class="rslides callbacks callbacks1" id="slider4">
							<li class="callbacks1_on" style="display: block; float: left; position: relative; opacity: 1; z-index: 2; transition: opacity 500ms ease-in-out;">
								<div class="banner-info">
								<h3>TODA LA INFORMACION DE LOS PLAYOFFS </h3>
								</div>
							</li>
							<li class="" style="display: block; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out;">
								<div class="banner-info">
								<h3>"WHERE AMAZING HAPPENS" </h3>	
								</div>
							</li>
						</ul>
						</div>
						<!--banner-Slider-->
						<script src="assets/js/responsiveslides.min.js"></script>
						 <script>
						// You can also use "$(window).load(function() {"
						$(function () {
						  // Slideshow 4
						  $("#slider4").responsiveSlides({
						auto: true,
						pager: true,
						nav:false,
						speed: 500,
						namespace: "callbacks",
						before: function () {
						  $('.events').append("<li>before event fired.</li>");
						},
						after: function () {
						  $('.events').append("<li>after event fired.</li>");
						}
						  });

						});
						  </script>
				</div>
  </div>
    <!--//end-banner-->
     <!--/start-main-->
          <div class="main-content">
		      <!--/soccer-inner-->
	                 <div class="soccer-inner">
					  <!--/soccer-left-part-->
					  <div class="col-md-8 soccer-left-part">
					    <!--/about-->
					      <div class="about">
						      <div class="sap_tabs">	
							  <div class="ab-sec">	
								   <div class="ab-inner">
								         <h4>Sobre los playoffs</h4>
                                         <p> Los Playoffs de la NBA son 3 rondas de competici�n entre diecis�is equipos repartidos en la Conferencia Oeste y la Conferencia Este. Los ganadores de la Primera ronda (o cuartos de final de conferencia) avanzan a las Semifinales de Conferencia, posteriormente a las Finales de Conferencia y los vencedores a las Finales de la NBA, disputadas entre los campeones de cada conferencia.
                                         A continuaci�n ver�s los equipos que se clasificaron para los Playoffs de la temporada 2014/15.</p>
									 	
									 </div>
							    </div>
							    </div>								
							</div>
							   <!--//about-->
							      <!--/players-->
							<div class="players">
							   <h3 class="tittle">Conferencia Este</h3>
								<ul id="flexiselDemo3">
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#one">
																	  <img src="assets/images/hawks.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="one">
																		  <img src="assets/images/hawks.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#two">
																	  <img src="assets/images/celtics.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="two">
																		 <img src="assets/images/celtics.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#three">
																	 <img src="assets/images/bucks.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="three">
																		  <img src="assets/images/bucks.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#four">
																	 <img src="assets/images/bulls.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="four">
																		  <img src="assets/images/bulls.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
														<li>
															<div class="biseller-column">
															<a class="lightbox" href="#five">
																		   <img src="assets/images/wizards.jpg" alt=""/>
																		</a> 
																		<div class="lightbox-target" id="five">
																			 <img src="assets/images/wizards.jpg" alt=""/>
																		   <a class="lightbox-close" href="#"> </a>

																			<div class="clearfix"> </div>
																		</div>
															</div>
														</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#six">
																	 <img src="assets/images/raptors.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="six">
																		   <img src="assets/images/raptors.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#seven">
																	   <img src="assets/images/cavs.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="seven">
																		 <img src="assets/images/cavs.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
																	<a class="lightbox" href="#eight">
																		<img src="assets/images/nets.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="eight">
																	 <img src="assets/images/nets.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>
																		
																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
											</ul>
										</div>
										<div class="players">
							   <h3 class="tittle">Conferencia Oeste</h3>
								<ul id="flexiselDemo4">
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#nine">
																	  <img src="assets/images/warriors.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="nine">
																		  <img src="assets/images/warriors.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#ten">
																	  <img src="assets/images/spurs.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="ten">
																		 <img src="assets/images/spurs.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#eleven">
																	 <img src="assets/images/clippers.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="eleven">
																		  <img src="assets/images/clippers.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#twelve">
																	 <img src="assets/images/grizzlies.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="twelve">
																		  <img src="assets/images/grizzlies.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
														<li>
															<div class="biseller-column">
															<a class="lightbox" href="#thirteen">
																		   <img src="assets/images/dallas.jpg" alt=""/>
																		</a> 
																		<div class="lightbox-target" id="thirteen">
																			 <img src="assets/images/dallas.jpg" alt=""/>
																		   <a class="lightbox-close" href="#"> </a>

																			<div class="clearfix"> </div>
																		</div>
															</div>
														</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#fourteen">
																	 <img src="assets/images/blazers.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="fourteen">
																		   <img src="assets/images/blazers.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
														<a class="lightbox" href="#fifteen">
																	   <img src="assets/images/rockets.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="fifteen">
																		 <img src="assets/images/rockets.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>

																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
													<li>
														<div class="biseller-column">
																	<a class="lightbox" href="#sixteen">
																		<img src="assets/images/pelicans.jpg" alt=""/>
																	</a> 
																	<div class="lightbox-target" id="sixteen">
																	 <img src="assets/images/pelicans.jpg" alt=""/>
																	   <a class="lightbox-close" href="#"> </a>
																		
																		<div class="clearfix"> </div>
																	</div>
														</div>
													</li>
											</ul>
										</div>
							<!--//players-->
						<script type="text/javascript">
							 $(window).load(function() {
								$("#flexiselDemo3").flexisel({
									visibleItems:3,
									animationSpeed: 1000,
									autoPlay: true,
									autoPlaySpeed: 3000,    		
									pauseOnHover: true,
									enableResponsiveBreakpoints: true,
									responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems:3
										}, 
										landscape: { 
											changePoint:640,
											visibleItems:3
										},
										tablet: { 
											changePoint:768,
											visibleItems:3
										}
									}
								});
								
							});
						   </script>
						   <script type="text/javascript">
							 $(window).load(function() {
								$("#flexiselDemo4").flexisel({
									visibleItems:3,
									animationSpeed: 1000,
									autoPlay: true,
									autoPlaySpeed: 3000,    		
									pauseOnHover: true,
									enableResponsiveBreakpoints: true,
									responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems:3
										}, 
										landscape: { 
											changePoint:640,
											visibleItems:3
										},
										tablet: { 
											changePoint:768,
											visibleItems:3
										}
									}
								});
								
							});
						   </script>
						   <script type="text/javascript" src="assets/js/jquery.flexisel.js"></script>
                     <!--//players-->
					  <!--/video-->
							<div class="video">
							   <h3 class="tittle">Video Promocional</h3>
							  <iframe src="https://player.vimeo.com/video/155919363?color=e3b92d&portrait=0"></iframe>
							</div>
							  <!--//video-->

					<!--banner Slider starts Here-->
					<script src="assets/js/responsiveslides.min.js"></script>
						 <script>
							// You can also use "$(window).load(function() {"
							$(function () {
							  // Slideshow 3
							  $("#slider3").responsiveSlides({
								auto: true,
								pager:false,
								nav: true,
								speed: 500,
								namespace: "callbacks",
								before: function () {
								  $('.events').append("<li>before event fired.</li>");
								},
								after: function () {
								  $('.events').append("<li>after event fired.</li>");
								}
							  });
						
							});
						  </script>
					 </div>
					<!--//soccer-left-part-->
							<!--/soccer-right-part-->
							<div class="col-md-4 soccer-right-part">
			                     <div class="modern">
							  <h4 class="side">Jugadores destacados</h4>
							  <div id="example1">
								<div id="owl-demo" class="owl-carousel text-center">
								  <div class="item">
							
									<img class="img-responsive lot" src="assets/images/curry.jpg" alt=""/>
								 </div>
								 <div class="item">
							
									<img class="img-responsive lot" src="assets/images/lebron.jpg" alt=""/>
								</div>
								<div class="item">
							
									<img class="img-responsive lot" src="assets/images/howard.jpg" alt=""/>
								</div>
								<div class="item">
							
									<img class="img-responsive lot" src="assets/images/iguodala.jpg" alt=""/>
							</div>
							<div class="item">
							
									<img class="img-responsive lot" src="assets/images/duncan.jpg" alt=""/>
							</div>
						</div>
				    </div>
				<!-- requried-jsfiles-for owl -->
										<script src="assets/js/owl.carousel.js"></script>
										  <script>
										  $(document).ready(function() {
											   $("#owl-demo").owlCarousel({
												items :1,
												lazyLoad : true,
												autoPlay : false,
												navigation : true,
												navigationText :  true,
												pagination : false,
												responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems: 2
										}, 
										landscape: { 
											changePoint:640,
											visibleItems: 2
										},
										tablet: { 
											changePoint:768,
											visibleItems: 3
										}
									}
												});
										  });
										</script>
						<!-- //requried-jsfiles-for owl -->
						</div>
				 <!--//accordation_menu-->
        
		 <!--//accordation_menu-->
		 <!--/top-news-->
			  <div class="top-news">
								 <h4 class="side">Partidos Destacados</h4>
							      <div class="top-inner">
								     <div class="top-text">
										 <a href="#"><img src="assets/images/game1.jpg" class="img-responsive" alt=""/></a>
										 <h5 class="top"><a href="#">San Antonio Spurs VS Los Angeles Clippers</a></h5>
										 <p>On Jun 25 <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>0 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>56 </a></p>
								     </div>
									  <div class="top-text two">
										 <a href="#"><img src="assets/images/game2.jpg" class="img-responsive" alt=""/></a>
										 <h5 class="top"><a href="#">Golden State Warriors VS Cleveland Cavaliers</a></h5>
										 <p>On Jun 25 <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>0 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>56 </a></p>
										
								     </div>
								  </div>
	                            </div>
							<!--//top-news-->
										<div class="connect">
											<h4 class="side">Siguenos</h4>
											  <ul class="stay">
												<li class="c5-element-facebook"><a href="#"><span class="icon"></span><span class="text">Facebook</span></a></li>
												<li class="c5-element-twitter"><a href="#"><span class="icon1"></span><span class="text">Twitter</span></a></li>
												 <li class="c5-element-gg"><a href="#"><span class="icon2"></span><span class="text">Google</span></a></li>
												
											  </ul>
										  </div>
										  	<!--//connect-->
		            </div>
			         <!--//soccer-right-part-->
		           <div class="clearfix"> </div>
	           </div>
			</div>
		<!--//soccer-inner-->
	 </div>
	<!--/start-footer-section-->

	<!--//end-footer-section-->
			<!--/start-copyright-section-->
				<div class="copyright">
						  <p>&copy; 2015 Basketball. All Rights Reserved | Design by <a href="http://w3layouts.com/">W3layouts</a> </p>
					</div>


				<!--start-smoth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>


<!--JS-->
<script type="text/javascript" src="assets/js/bootstrap-3.1.1.min.js"></script>

<!--//JS-->

</body>
</html>