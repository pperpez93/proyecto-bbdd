<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Consultas</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Basketball Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="assets/css/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css">
<!-- Custom Theme files -->
<link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="assets/css/style.css" rel='stylesheet' type='text/css' />	
<script src="assets/js/jquery.min.js"> </script>
<script type="text/javascript" src="assets/js/move-top.js"></script>
<script type="text/javascript" src="assets/js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
</head>
<body> 
 <div class="container">
  <div class="header" id="home">
	 <div class="subhead white">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp"><h1>NBA Playoffs<span> 2015</span></h1> </a>
			</div>
			<!--/.navbar-header-->

					<div class="collapse navbar-collapse pull-right"
						id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="consultas.jsp">Consultas</a></li>
							<li><a href="galeria.jsp">Galer�a</a></li>
						</ul>
					</div>
					<!--/.navbar-collapse-->
	 <!--/.navbar-->
	</nav>
  </div>
 </div>
   <!--/start-banner-->
    <div class="banner two">
  </div>
    <!--//end-banner-->
     <!--/start-main-->
          <div class="main-content">
		      <!--/soccer-inner-->
	                 <div class="soccer-inner">
					  <!--/soccer-left-part-->
					  <div class="col-md-8 soccer-left-part blog">
					    <!--/posts-->
					    <div class="blogs">
						    <h3 class="tittle">Consultas</h3>
					        <div class="blog-grid">
							    <a href="single.html"> <img src="assets/images/query1.jpg" class="img-responsive" alt="" /></a>
								 <div class="blog-info second">
									   <a class="news" href="query1.jsp"> JUGADORES SEG�N EQUIPO</a>
									  <p>Echa un vistazo a los partidos de cada equipo </p>
										  </div>
						     </div>
							 <div class="blog-grid two">
							    <a href="single.html"> <img src="assets/images/query2.jpg" class="img-responsive" alt="" /></a>
								 <div class="blog-info second">
									   <a class="news" href="query2.jsp"> NUMERO DE VICTORIAS DE CADA EQUIPO COMO VISITANTE</a>
									  <p>Echa un vistazo al n�mero de partidos que ha ganado cada equipo siendo visitante </p>
										  </div>
						     </div>
							 <div class="blog-grid two">
							    <a href="single.html"> <img src="assets/images/query3.jpeg" class="img-responsive" alt="" /></a>
								 <div class="blog-info second">
									   <a class="news" href="query3.jsp"> JUGADORES DEL OESTE CON M�S DE 20 PUNTOS</a>
									  <p>Aqu� ver�s qu� jugadores han promediado, durante los Playoffs, m�s de 20 puntos </p>
										  </div>
						     </div>
						     <div class="blog-grid two">
							    <a href="single.html"> <img src="assets/images/query4.jpg" class="img-responsive" alt="" /></a>
								 <div class="blog-info second">
									   <a class="news" href="query4.jsp"> N�MERO DE BASES CON 5 ASISTENCIAS O M�S</a>
									  <p>Comprueba cu�ntos bases de cada equipo han promediado 5 o m�s asistencias </p>
										  </div>
						     </div>
						     <div class="blog-grid two">
							    <a href="single.html"> <img src="assets/images/query5.jpg" class="img-responsive" alt="" /></a>
								 <div class="blog-info second">
									   <a class="news" href="query5.jsp"> PARTIDOS SEG�N EQUIPO</a>
									  <p>Echa un vistazo a los partidos de cada equipo </p>
										  </div>
						     </div>
						     <div class="blog-grid two">
							    <a href="single.html"> <img src="assets/images/query6.jpg" class="img-responsive" alt="" /></a>
								 <div class="blog-info second">
									   <a class="news" href="query6.jsp"> EQUIPOS CON M�S DE 5 PARTIDOS</a>
									  <p>Solo 11 equipos han disputado m�s de 5 partidos </p>
										  </div>
						     </div>
						</div>
						<!--//posts-->
							      
					<!--banner Slider starts Here-->
					<script src="assets/js/responsiveslides.min.js"></script>
						 <script>
							// You can also use "$(window).load(function() {"
							$(function () {
							  // Slideshow 3
							  $("#slider3").responsiveSlides({
								auto: true,
								pager:false,
								nav: true,
								speed: 500,
								namespace: "callbacks",
								before: function () {
								  $('.events').append("<li>before event fired.</li>");
								},
								after: function () {
								  $('.events').append("<li>after event fired.</li>");
								}
							  });
						
							});
						  </script>
					 </div>
					<!--//soccer-left-part-->
							<!--/soccer-right-part-->
					<div class="col-md-4 soccer-right-part">
			                     <div class="modern">
							  <h4 class="side">Jugadores destacados</h4>
							  <div id="example1">
								<div id="owl-demo" class="owl-carousel text-center">
								  <div class="item">
							
									<img class="img-responsive lot" src="assets/images/curry.jpg" alt=""/>
								 </div>
								 <div class="item">
							
									<img class="img-responsive lot" src="assets/images/lebron.jpg" alt=""/>
								</div>
								<div class="item">
							
									<img class="img-responsive lot" src="assets/images/howard.jpg" alt=""/>
								</div>
								<div class="item">
							
									<img class="img-responsive lot" src="assets/images/iguodala.jpg" alt=""/>
							</div>
							<div class="item">
							
									<img class="img-responsive lot" src="assets/images/duncan.jpg" alt=""/>
							</div>
						</div>
				    </div>
				<!-- requried-jsfiles-for owl -->
										<script src="assets/js/owl.carousel.js"></script>
										  <script>
										  $(document).ready(function() {
											   $("#owl-demo").owlCarousel({
												items :1,
												lazyLoad : true,
												autoPlay : false,
												navigation : true,
												navigationText :  true,
												pagination : false,
												responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems: 2
										}, 
										landscape: { 
											changePoint:640,
											visibleItems: 2
										},
										tablet: { 
											changePoint:768,
											visibleItems: 3
										}
									}
												});
										  });
										</script>
						<!-- //requried-jsfiles-for owl -->
						</div>
				 <!--//accordation_menu-->
        
		 <!--//accordation_menu-->
		 <!--/top-news-->
			  <div class="top-news">
								 <h4 class="side">Partidos Destacados</h4>
							      <div class="top-inner">
								     <div class="top-text">
										 <a href="#"><img src="assets/images/game1.jpg" class="img-responsive" alt=""/></a>
										 <h5 class="top"><a href="#">San Antonio Spurs VS Los Angeles Clippers</a></h5>
										 <p>On Jun 25 <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>0 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>56 </a></p>
								     </div>
									  <div class="top-text two">
										 <a href="#"><img src="assets/images/game2.jpg" class="img-responsive" alt=""/></a>
										 <h5 class="top"><a href="#">Golden State Warriors VS Cleveland Cavaliers</a></h5>
										 <p>On Jun 25 <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>0 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>56 </a></p>
										
								     </div>
								  </div>
	                            </div>
							<!--//top-news-->
										<div class="connect">
											<h4 class="side">Siguenos</h4>
											  <ul class="stay">
												<li class="c5-element-facebook"><a href="#"><span class="icon"></span><span class="text">Facebook</span></a></li>
												<li class="c5-element-twitter"><a href="#"><span class="icon1"></span><span class="text">Twitter</span></a></li>
												 <li class="c5-element-gg"><a href="#"><span class="icon2"></span><span class="text">Google</span></a></li>
												
											  </ul>
										  </div>
										  	<!--//connect-->
		            </div>
			         <!--//soccer-right-part-->
		           <div class="clearfix"> </div>
	           </div>
			</div>
		<!--//soccer-inner-->
	 </div>
	<!--/start-footer-section-->

	<!--//end-footer-section-->
			<!--/start-copyright-section-->
				<div class="copyright">
						  <p>&copy; 2015 Basketball. All Rights Reserved | Design by <a href="http://w3layouts.com/">W3layouts</a> </p>
					</div>


				<!--start-smoth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>


<!--JS-->
<script type="text/javascript" src="assets/js/bootstrap-3.1.1.min.js"></script>

<!--//JS-->

</body>
</html>