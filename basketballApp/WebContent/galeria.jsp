<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Galer�a</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Basketball Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="assets/css/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css">
<!-- Custom Theme files -->
<link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="assets/css/style.css" rel='stylesheet' type='text/css' />	
<script src="assets/js/jquery.min.js"> </script>
<script type="text/javascript" src="assets/js/move-top.js"></script>
<script type="text/javascript" src="assets/js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
</head>
<body> 
 <div class="container">
  <div class="header" id="home">
	 <div class="subhead white">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp"><h1>NBA Playoffs<span> 2015</span></h1> </a>
			</div>
			<!--/.navbar-header-->

					<div class="collapse navbar-collapse pull-right"
						id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="consultas.jsp">Consultas</a></li>
							<li><a href="galeria.jsp">Galer�a</a></li>
						</ul>
					</div>
					<!--/.navbar-collapse-->
	 <!--/.navbar-->
	</nav>
  </div>
 </div>
   <!--/start-banner-->
    <div class="banner two">
  </div>
    <!--//end-banner-->
     <!--/start-main-->
     <!--/start-main-->
          <div class="main-content">
		     <!--/gallery-->
			 <div class="gallery">
					<h3 class="tittle">Galer�a</h3>
					<div class="gallery-bottom grid">
						<div class="col-md-3 g-left">
								<a href="assets/images/playoffs1.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs1.jpg" alt=""/>		
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs2.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs2.jpg" alt=""/>		
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs3.png" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs3.png" alt=""/>		
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs4.png" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs4.png" alt=""/>			
									</figure>
								</a>
							</div>
								<div class="col-md-3 g-left">
								<a href="assets/images/playoffs5.png" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs5.png" alt=""/>		
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs6.png" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs6.png" alt=""/>		
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs7.png" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs7.png" alt=""/>			
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs8.png" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs8.png" alt=""/>		
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs9.png" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs9.png" alt=""/>		
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs10.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs10.jpg" alt=""/>			
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs11.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs11.jpg" alt=""/>		
									</figure>
								</a>
							</div>
							<div class="col-md-3 g-left">
								<a href="assets/images/playoffs12.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
									<figure class="effect-oscar">
										<img src="assets/images/playoffs12.jpg" alt=""/>			
									</figure>
								</a>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
			</div>

	         <!--//gallery-->
	</div>
</div>
	<!--/start-footer-section-->

	<!--//end-footer-section-->
			<!--/start-copyright-section-->
				<div class="copyright">
						  <p>&copy; 2015 Basketball. All Rights Reserved | Design by <a href="http://w3layouts.com/">W3layouts</a> </p>
					</div>


				<!--start-smoth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>


<!--JS-->
<script type="text/javascript" src="assets/js/bootstrap-3.1.1.min.js"></script>

<!--//JS-->

</body>
</html>